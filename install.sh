BASE=`dirname $0`

echo "* Installing .vimrc"
ln -vs $BASE/vimrc ~/.vimrc

echo "* Installing .zshrc"
ln -vs $BASE/zshrc ~/.zshrc

echo "* Installing git config files"
ln -vs $BASE/gitignore ~/.gitignore
ln -vs $BASE/gitconfig ~/.gitconfig

echo "Done"
