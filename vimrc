set nocompatible
filetype plugin on
syntax on

" File browsing
set path+=**
set wildmenu

"autocmd vimenter * ++nested colorscheme gruvbox

set updatetime=500

" Status
set laststatus=2
set statusline=%F%m%r%h%w%=\ [%Y]\ [%{&ff}]\ [%04l,%04v]\ [%p%%]\ [%L]

set background=dark

set number
set relativenumber
set cursorline
set splitbelow
set splitright

" Indents
set expandtab
set smartindent
set smarttab
set tabstop=2
set softtabstop=2
set shiftwidth=2

" Folds
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

" Line length
set wrap
"set textwidth=80
set colorcolumn=81

" Search
set incsearch
set hlsearch

set omnifunc=syntaxcomplete#Complete

set mouse=a
